/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "mpsc_vinfarr.hh"
#include "mpsc_tail_swap.hh"
#include "ut_helpers.hh"
#include <thread>
#include <time.h>
#include <memory>

#include "gtest/gtest.h"

using mt_queue::MPSC_VirtInfArray;
using mt_queue::MPSC_TailSwap;
using mt_queue::TuneMemoryPoolFlag;
using mt_queue::TuneDeletePayloadFlag;
using std::thread;


TEST(VirtInfArray, Create) {
    MPSC_VirtInfArray<void*> myqueue1;
    MPSC_VirtInfArray<char*> myqueue2;
    MPSC_VirtInfArray<void*, TuneDeletePayloadFlag<true>> myqueue3;
    MPSC_VirtInfArray<char*, TuneDeletePayloadFlag<true>> myqueue4;
}


TEST(MPSC_TailSwap, CreateAndDestroy) {
    MPSC_TailSwap<void*> myqueue5;
    MPSC_TailSwap<std::unique_ptr<char*>> myqueue6;
}


TEST(VirtInfArray, EnqueueDequeueSomeItems) {
    {
        MPSC_VirtInfArray<void*> myqueue;

        for (uintptr_t i = 1; i <= 10000; ++i) {
            myqueue.enqueue((void*)i);
        }

        for (uintptr_t i = 1; i <= 10000; ++i) {
            /* If item was a pointer
             * then the queue returns it as a return value */
            void* j = myqueue.dequeue();
            EXPECT_EQ(j, (void*)i);
        }

        /* If item was a pointer and the queue is empty
         * then dequeue returns nullptr */
        void* noitem = myqueue.dequeue();
        EXPECT_EQ(noitem, nullptr);
    }

    {
        MPSC_VirtInfArray<char*> myqueue;

        for (uintptr_t i = 1; i <= 10000; ++i) {
            myqueue.enqueue((char*)i);
        }

        for (uintptr_t i = 1; i <= 10000; ++i) {
            char* j = myqueue.dequeue();
            EXPECT_EQ(j, (void*)i);
        }

        char* noitem = myqueue.dequeue();
        EXPECT_EQ(noitem, nullptr);
    }

    {
        union ItemType {
            int Array[10];
            size_t num;
        };

        MPSC_VirtInfArray<ItemType> myqueue;

        for (uintptr_t i = 1; i <= 10; ++i) {
            ItemType item;
            item.num = i;
            myqueue.enqueue(item);
        }

        for (uintptr_t i = 1; i <= 10; ++i) {
            ItemType j;
            bool ready = myqueue.dequeue(&j);
            EXPECT_TRUE(ready);
            EXPECT_EQ(j.num, i);
        }

        ItemType a;
        bool ready = myqueue.dequeue(&a);
        EXPECT_FALSE(ready);
    }

    {
        union ItemType {
            int Array[10];
            struct {
                size_t padding;
                size_t num;
            };
        };

        MPSC_VirtInfArray<int[10]> myqueue;

        for (uintptr_t i = 1; i <= 10; ++i) {
            ItemType item;
            item.num = i;
            myqueue.enqueue(item.Array);
        }

        for (uintptr_t i = 1; i <= 10; ++i) {
            ItemType j;
            bool ready = myqueue.dequeue(j.Array);
            EXPECT_TRUE(ready);
            EXPECT_EQ(j.num, i);
        }

        ItemType a;
        bool ready = myqueue.dequeue(a.Array);
        EXPECT_FALSE(ready);
    }

}


TEST(MPSC_TailSwap, EnqueueDequeueSomeItems) {
    {
        MPSC_TailSwap<void*> myqueue;

        for (uintptr_t i = 1; i <= 10000; ++i) {
            myqueue.enqueue((void*)i);
        }

        for (uintptr_t i = 1; i <= 10000; ++i) {
            /* If item was a pointer
             * then the queue returns it as a return value */
            void* item;
            bool ready = myqueue.dequeue(item);
            EXPECT_TRUE(ready);
            EXPECT_EQ(item, (void*)i);
        }

        /* If item was a pointer and the queue is empty
         * then dequeue returns nullptr */
        void* noitem = (void*)0xEFAB;
        bool ready = myqueue.dequeue(noitem);
        EXPECT_FALSE(ready);
        EXPECT_EQ(noitem, (void*)0xEFAB);
    }
}


TEST(VirtInfArray, Enqueue3Thread_Dequeue1Thread_PointerElem) {
    constexpr int NUMBER_OF_PRODUCERS = 3;
    constexpr int NUMBER_OF_ITEMS = 1000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    MPSC_VirtInfArray<void*> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);

    auto producer_lambda = [&](int start_num) {
        sync.WaitForGreenLight();
        for (int i = 1; i <= NUMBER_OF_ITEMS; ++i)
            myqueue.enqueue((void*)(uintptr_t)(i + start_num));
    };

    thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        producers[i] = thread(producer_lambda, i * DIFF_GAP);
        last_items[i] = i * DIFF_GAP;
    }

    sync.Start();
    for (size_t item_count = 0;
         item_count < NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;)
    {
        void* item = myqueue.dequeue();
        if (item == nullptr)
            continue;

        uintptr_t item_int = reinterpret_cast<uintptr_t>(item);
        uintptr_t producer_num = item_int / DIFF_GAP;
        EXPECT_EQ(last_items[producer_num] + 1, item_int);
        last_items[producer_num] = item_int;
        ++item_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(VirtInfArray, Enqueue3Thread_Dequeue1Thread_StructElem) {
    constexpr int NUMBER_OF_PRODUCERS = 3;
    constexpr int NUMBER_OF_ITEMS = 1000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    union ItemType {
        int Array[10];
        size_t num;
    };

    MPSC_VirtInfArray<ItemType> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);

    auto producer_lambda = [&](int start_num) {
        sync.WaitForGreenLight();
        for (int i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            ItemType item;
            item.num = i + start_num;
            myqueue.enqueue(item);
        }
    };

    thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        producers[i] = thread(producer_lambda, i * DIFF_GAP);
        last_items[i] = i * DIFF_GAP;
    }

    sync.Start();
    for (size_t item_count = 0;
         item_count < NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;)
    {
        ItemType item;
        bool ready = myqueue.dequeue(&item);
        if (!ready)
            continue;

        size_t item_int = item.num;
        size_t producer_num = item_int / DIFF_GAP;
        EXPECT_EQ(last_items[producer_num] + 1, item_int);
        last_items[producer_num] = item_int;
        ++item_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(MPSC_TailSwap, Enqueue3Thread_Dequeue1Thread) {
    constexpr int NUMBER_OF_PRODUCERS = 3;
    constexpr int NUMBER_OF_ITEMS = 1000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    MPSC_TailSwap<void*> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);

    auto producer_lambda = [&](int start_num) {
        sync.WaitForGreenLight();
        for (int i = 1; i <= NUMBER_OF_ITEMS; ++i)
            myqueue.enqueue((void*)(uintptr_t)(i + start_num));
    };

    thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        producers[i] = thread(producer_lambda, i * DIFF_GAP);
        last_items[i] = i * DIFF_GAP;
    }

    sync.Start();
    for (size_t item_count = 0;
         item_count < NUMBER_OF_ITEMS * NUMBER_OF_PRODUCERS;)
    {
        void* item;
        bool ready = myqueue.dequeue(item);
        if (!ready)
            continue;

        uintptr_t item_int = reinterpret_cast<uintptr_t>(item);
        uintptr_t producer_num = item_int / DIFF_GAP;
        EXPECT_EQ(last_items[producer_num] + 1, item_int);
        last_items[producer_num] = item_int;
        ++item_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(VirtInfArray, CrossProducersOrder_PointerElem) {
    constexpr int NUMBER_OF_PRODUCERS = 6;
    constexpr int NUMBER_OF_ITEMS = 1000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    MPSC_VirtInfArray<void*> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);
    std::atomic<uint64_t> dependance_counter(0);

    auto producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t i = 1; i <= NUMBER_OF_ITEMS; ++i)
            myqueue.enqueue((void*)(uintptr_t)(start_num + i));
    };

    auto order_foo_producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            myqueue.enqueue((void*)(uintptr_t)(start_num + i));
            dependance_counter.store(i, std::memory_order_release);
        }
    };

    auto order_bar_producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t last_item = 0; last_item < NUMBER_OF_ITEMS;) {
            uint64_t item = dependance_counter.load(std::memory_order_acquire);
            if (item == last_item)
                continue;
            myqueue.enqueue((void*)(uintptr_t)(item + start_num));
            last_item = item;
        }
    };

    thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        switch (i) {
        case 0:
            producers[0] = thread(order_foo_producer_lambda, 0);
            last_items[0] = 0;
            continue;
        case 1:
            producers[1] = thread(order_bar_producer_lambda, DIFF_GAP);
            last_items[1] = DIFF_GAP;
            continue;
        default:
            producers[i] = thread(producer_lambda, i * DIFF_GAP);
            last_items[i] = i * DIFF_GAP;
            continue;
        }
    }

    sync.Start();
    for (size_t last_count = 0;
         last_count < NUMBER_OF_PRODUCERS;)
    {
        void* item = myqueue.dequeue();
        if (item == nullptr)
            continue;

        uintptr_t item_int = reinterpret_cast<uintptr_t>(item);
        uintptr_t producer_num = item_int / DIFF_GAP;
        if (producer_num == 1) {
            EXPECT_LT(last_items[producer_num], item_int);
            EXPECT_GE(last_items[0] + DIFF_GAP, item_int);
        } else {
            EXPECT_EQ(last_items[producer_num] + 1, item_int);
        }

        last_items[producer_num] = item_int;
        if (item_int == NUMBER_OF_ITEMS + DIFF_GAP * producer_num)
            ++last_count;
    }

    for (auto& producer: producers)
        producer.join();
}


TEST(VirtInfArray, CrossProducersOrder_StructElem) {
    constexpr int NUMBER_OF_PRODUCERS = 6;
    constexpr int NUMBER_OF_ITEMS = 1000000;
    constexpr int DIFF_GAP = 10 * NUMBER_OF_ITEMS;

    union ItemType {
        int Array[10];
        size_t num;
    };

    MPSC_VirtInfArray<ItemType> myqueue;
    SyncStart sync(NUMBER_OF_PRODUCERS);
    std::atomic<uint64_t> dependance_counter(0);

    auto producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            ItemType item;
            item.num = i + start_num;
            myqueue.enqueue(item);
        }
    };

    auto order_foo_producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t i = 1; i <= NUMBER_OF_ITEMS; ++i) {
            ItemType item;
            item.num = start_num + i;
            myqueue.enqueue(item);
            dependance_counter.store(i, std::memory_order_release);
        }
    };

    auto order_bar_producer_lambda = [&](uint64_t start_num) {
        sync.WaitForGreenLight();
        for (uint64_t last_num = 0; last_num < NUMBER_OF_ITEMS;) {
            uint64_t cnum = dependance_counter.load(std::memory_order_acquire);
            if (cnum == last_num)
                continue;
            ItemType item;
            item.num = cnum + start_num;
            myqueue.enqueue(item);
            last_num = cnum;
        }
    };

    thread producers[NUMBER_OF_PRODUCERS];
    uintptr_t last_items[NUMBER_OF_PRODUCERS];
    for (int i = 0; i < NUMBER_OF_PRODUCERS; ++i) {
        switch (i) {
        case 0:
            producers[0] = thread(order_foo_producer_lambda, 0);
            last_items[0] = 0;
            continue;
        case 1:
            producers[1] = thread(order_bar_producer_lambda, DIFF_GAP);
            last_items[1] = DIFF_GAP;
            continue;
        default:
            producers[i] = thread(producer_lambda, i * DIFF_GAP);
            last_items[i] = i * DIFF_GAP;
            continue;
        }
    }

    sync.Start();
    for (size_t last_count = 0;
         last_count < NUMBER_OF_PRODUCERS;)
    {
        ItemType item;
        bool ready = myqueue.dequeue(&item);
        if (!ready)
            continue;

        uintptr_t item_int = item.num;
        uintptr_t producer_num = item_int / DIFF_GAP;
        if (producer_num == 1) {
            EXPECT_LT(last_items[producer_num], item_int);
            EXPECT_GE(last_items[0] + DIFF_GAP, item_int);
        } else {
            EXPECT_EQ(last_items[producer_num] + 1, item_int);
        }

        last_items[producer_num] = item_int;
        if (item_int == NUMBER_OF_ITEMS + DIFF_GAP * producer_num)
            ++last_count;
    }

    for (auto& producer: producers)
        producer.join();
}


template <size_t SIZE>
class AllocatorMock: public mt_queue::I_FixedSizeAllocator {
public:
    virtual void* alloc() override {
        auto result = ::operator new(SIZE);
        //printf("allocated: %p\n", result);
        return result;
    }

    virtual void free(void* ptr) override {
        //printf("deleted: %p\n", ptr);
        return ::operator delete(ptr);
    }

    virtual size_t getBlockSize() override {
        return SIZE;
    }
};


TEST(VirtInfArrayMemPool, Create) {
    AllocatorMock<128> mempool;
    MPSC_VirtInfArray<void*, TuneMemoryPoolFlag<>> myqueue1(&mempool);
    MPSC_VirtInfArray<char*, TuneMemoryPoolFlag<>> myqueue2(&mempool);
    MPSC_VirtInfArray<void*, TuneMemoryPoolFlag<>, TuneDeletePayloadFlag<>>
        myqueue3(&mempool);
    MPSC_VirtInfArray<char*, TuneMemoryPoolFlag<>, TuneDeletePayloadFlag<>>
        myqueue4(&mempool);
    MPSC_VirtInfArray<void*, TuneDeletePayloadFlag<>, TuneMemoryPoolFlag<>>
        myqueue5(&mempool);
    MPSC_VirtInfArray<char*, TuneDeletePayloadFlag<>, TuneMemoryPoolFlag<>>
        myqueue6(&mempool);
}


TEST(VirtInfArrayMemPool, EnqueueDequeueSomeItems) {
    AllocatorMock<128> mempool;
    MPSC_VirtInfArray<void*, TuneMemoryPoolFlag<>> myqueue(&mempool);

    for (uintptr_t i = 1; i <= 10000; ++i) {
        myqueue.enqueue((void*)i);
    }

    for (uintptr_t i = 1; i <= 10000; ++i) {
        auto j = myqueue.dequeue();
        EXPECT_EQ(j, (void*)i);
    }

    void* noitem = myqueue.dequeue();
    EXPECT_EQ(noitem, nullptr);
}

#pragma once

namespace mt_queue {

template <typename BaseType,
          typename FuseType,
          typename ... MoreFuseTypes>
struct FuseParams
    : public FuseParams<
        typename FuseType::template ApplyTemplate<BaseType>,
        MoreFuseTypes...>
{
};

template <typename BaseType, typename FuseType>
struct FuseParams<BaseType, FuseType>
        : public FuseType::template ApplyTemplate<BaseType>
{
};


#define                                                                       \
    DECLARE_TUNE_VALUE_PARAM(TemplateParamName, ParamType, InStructParamName) \
    template <ParamType VALUE>                                                \
    struct TemplateParamName {                                                \
        template <typename BaseType>                                          \
        struct ApplyTemplate: public BaseType {                               \
            static constexpr ParamType InStructParamName = VALUE;             \
        };                                                                    \
    }


#define                                                           \
    DECLARE_TUNE_VALUE_PARAM_DEFVALUE(                            \
        TemplateParamName,                                        \
        ParamType,                                                \
        DefaultValue,                                             \
        InStructParamName)                                        \
    template <ParamType VALUE = DefaultValue>                     \
    struct TemplateParamName {                                    \
        template <typename BaseType>                              \
        struct ApplyTemplate: public BaseType {                   \
            static constexpr ParamType InStructParamName = VALUE; \
        };                                                        \
    }


#define DECLARE_TUNE_TYPE_PARAM(TemplateParamName, InStructParamName) \
    template <typename Value>                                         \
    struct TemplateParamName {                                        \
        template <typename BaseType>                                  \
        struct ApplyTemplate: public BaseType {                       \
            typedef Value InStructParamName;                          \
        };                                                            \
    }


#define DECLARE_TUNE_TYPE_PARAM_DEFTYPE(               \
    TemplateParamName, InStructParamName, DefaultType) \
    template <typename Value = DefaultType>            \
    struct TemplateParamName {                         \
        template <typename BaseType>                   \
        struct ApplyTemplate: public BaseType {        \
            typedef Value InStructParamName;           \
        };                                             \
    }

}

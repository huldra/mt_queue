/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "ut_helpers.hh"
#include <thread>
#include <atomic>
#include <time.h>
#include "gtest/gtest.h"

#include "spmc_array.hh"

using std::thread;
using std::atomic;
using mt_queue::SPMC_Array;

TEST(SPMC_Array, Create) {
    SPMC_Array<void, false> queue(16);
}


TEST(SPMC_Array, Capacity) {
    constexpr size_t QUEUE_SIZE = 16;
    SPMC_Array<void, false> queue(QUEUE_SIZE);
    for (size_t i = 0; i < QUEUE_SIZE; ++i)
        EXPECT_TRUE(queue.enqueue((void*)1));

    for (size_t i = 0; i < QUEUE_SIZE * 2; ++i)
        EXPECT_FALSE(queue.enqueue((void*)1));
}


TEST(SPMC_Array, EnqueueDequeue) {
    constexpr size_t QUEUE_SIZE = 16;
    SPMC_Array<void, false> queue(QUEUE_SIZE);
    for (int j = 0; j < 10; ++j) {
        for (size_t i = 0; i < QUEUE_SIZE; ++i)
            EXPECT_TRUE(queue.enqueue((void*)1));

        for (size_t i = 0; i < QUEUE_SIZE * 2; ++i)
            EXPECT_FALSE(queue.enqueue((void*)1));

        for (size_t i = 0; i < QUEUE_SIZE; ++i)
            EXPECT_EQ((void*)1, queue.dequeue());

        for (size_t i = 0; i < QUEUE_SIZE * 2; ++i)
            EXPECT_EQ(nullptr, queue.dequeue());
    }

    for (size_t i = 0; i < QUEUE_SIZE / 2; ++i)
        EXPECT_TRUE(queue.enqueue((void*)1));

    for (int j = 0; j < 10; ++j) {
        for (size_t i = 0; i < 2; ++i)
            EXPECT_TRUE(queue.enqueue((void*)1));

        for (size_t i = 0; i < 2; ++i)
            EXPECT_EQ((void*)1, queue.dequeue());
    }

    for (size_t i = 0; i < QUEUE_SIZE / 2; ++i)
        EXPECT_EQ((void*)1, queue.dequeue());
}


TEST(SPMC_Array, MT_SPSC) {
    constexpr size_t QUEUE_SIZE = 16;
    constexpr size_t ITEMS_COUNT = 100000;
    SPMC_Array<void, false> queue(QUEUE_SIZE);

    auto producer_lambda = [&]() {
        size_t sent = 0;
        do {
            if (queue.enqueue((void*)(1 + sent)))
                ++sent;
        } while (sent < ITEMS_COUNT);
    };

    auto consumer_lambda = [&]() {
        size_t received = 0;
        do {
            void* item = queue.dequeue();
            if (item == nullptr)
                continue;
            ++received;
            EXPECT_EQ(item, (void*)received);
        } while (received < ITEMS_COUNT);
    };

    for (int pass = 0; pass < 10; ++pass) {
        std::thread consumer_thread(consumer_lambda);
        std::thread producer_thread(producer_lambda);

        consumer_thread.join();
        producer_thread.join();
    }
}


TEST(SPMC_Array, MT_SPMC) {
    constexpr size_t QUEUE_SIZE = 16;
    constexpr size_t ITEMS_COUNT = 100000;
    constexpr size_t NUMBER_OF_CONSUMERS = 3;
    SPMC_Array<void, false> queue(QUEUE_SIZE);
    atomic<size_t> total_sum(0);
    atomic<bool> exit_flag(false);

    auto producer_lambda = [&]() {
        size_t sent = 0;
        do {
            if (queue.enqueue((void*)(1 + sent)))
                ++sent;
        } while (sent < ITEMS_COUNT);

        exit_flag.store(true, std::memory_order_release);
    };

    auto consumer_lambda = [&]() {
        size_t local_sum = 0;
        //void* prev_item = nullptr;

        for (;;) {
            void* item = queue.dequeue();
            if (item == nullptr) {
                if (exit_flag.load(std::memory_order_acquire)) {
                    item = queue.dequeue();
                    if (item == nullptr)
                        break;
                } else {
                    continue;
                }
            }
            //EXPECT_GT(item, prev_item);
            local_sum += (uintptr_t)item;
        };

        total_sum.fetch_add(local_sum, std::memory_order_seq_cst);
    };

    for (int pass = 0; pass < 10; ++pass) {
        thread consumer_thread[NUMBER_OF_CONSUMERS];
        for (size_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
            consumer_thread[i] = thread(consumer_lambda);
        thread producer_thread(producer_lambda);

        for (size_t i = 0; i < NUMBER_OF_CONSUMERS; ++i)
            consumer_thread[i].join();
        producer_thread.join();

        EXPECT_EQ(total_sum.load(std::memory_order_acquire),
                  (ITEMS_COUNT * (ITEMS_COUNT + 1)) / 2);
        total_sum.store(0);
        exit_flag.store(false);
    }
}

# Collection of wait-free/lock-free queues

## mpsc_vinfarr

This is a multiple producers single consumer queue. The queue is wait-free
on x86 and amd64 platforms (actually if fetch_add (xadd instruction)
is wait-free) and if memory allocator is wait-free.

fetch_add seems to be not wait-free on aarch64, arm, mips, mips64, PowerPC and
many other platforms. That's why the queue is not wait-free (but lock-free)
on these platforms.

The queue uses virtual infinite array represented by double-linked list
of chunks, each chunk contains several slots for payload data. Payload data
must be a pointer to an address in memory. nullptr is reserved for internal use
and must not be enqueued.

Consumer never blocks and receives nullptr in the result if no more items
were available. Consumer has wait-free bounded guarantee for its progress.

Producer has wait-free bounded guarantee in case a producer had no delays
(interruptions or freezed-by-cpu-scheduler events) in its execution. In case of
the delays producer has wait-free unbounded guarantee. The time of execution
depends on how long it had been delayed.

The queue uses "stamp-it"-like memory reclamation technic to free unused chunks
based on a counter of enqueued items. Each time a producer advances the tail
to a next chunk it reads "TailCounter", a total number of slots allocated
by producers, and save it into special field "ReleaseCounter" in the next chunk.
When a consumer successfully read all "ReleaseCounter" items, it is safe
to free all previous chunks. The reclamation technic is not lock-free.


## spmc_array

This is a single producers multiple consumers bounded queue.
It is implemented by array of elemenets. It uses fetch_add atomic
operatation. It is wait-free on x86 and amd64 platforms. But it seems to be
lock-free on all other platforms, including arm, aarch64, PowerPC, MIPS,
MIPS64, etc.

Consumers never blocks and receives nullptr in the result
if no more items were available.

Producer never blocks and receives false if no more space were available.
